<?php
namespace App\EmailAddress;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class EmailAddress extends DB
{
    private $id;
    private $name;
    private $email;




    public function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id= $allPostData['id'];
        }

        if(array_key_exists("Name",$allPostData)){
            $this->name= $allPostData['Name'];
        }

        if(array_key_exists("Email",$allPostData)){
            $this->email= $allPostData['Email'];
        }


    }


    public function store(){
       $arrData  =  array($this->name,$this->email);

       $query= 'INSERT INTO email_address (name, email) VALUES (?,?)';

       $STH = $this->DBH->prepare($query);
       $result = $STH->execute($arrData);

       if($result){
           Message::setMessage("Success! Data has been stored successfully!");
       }
       else{
           Message::setMessage("Failed! Data has not been stored!");
       }

        Utility::redirect('index.php');

    }

    public function view(){

        $sql = "Select * from email_address where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function index(){

        $sql = "Select * from email_address where soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function trashed(){

        $sql = "Select * from email_address where soft_deleted='Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }



    public function update(){
        $arrData  =  array($this->name,$this->email);

        $query= 'UPDATE email_address SET name = ?, email= ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been updated!");
        }

        Utility::redirect('index.php');

    }



    public function trash(){
        $arrData  =  array("Yes");

        $query= 'UPDATE email_address SET soft_deleted = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been successfully Trashed!");
        }
        else{
            Message::setMessage("Failed! Data has not been Trashed!");
        }

        Utility::redirect('trashed.php');

    }




    public function recover(){
        $arrData  =  array("No");

        $query= 'UPDATE email_address SET soft_deleted = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been Recover successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recoverd!");
        }

        Utility::redirect('index.php');

    }


    public function delete(){
        $sql = "DELETE from email_address WHERE id=".$this->id;

        $result= $this->DBH->exec($sql);


        if($result){
            Message::setMessage("Success! Data has been Recover successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recoverd!");
        }
        Utility::redirect('index.php');
    }



    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from email_address  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }





    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from email_address  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `email_address` WHERE `soft_deleted` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `email_address` WHERE `soft_deleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `email_address` WHERE `soft_deleted` ='No' AND `email` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->email);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->email);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



}












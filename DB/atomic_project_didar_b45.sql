-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2017 at 07:29 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_didar_b45`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Bangla', 'Didar', 'No'),
(3, 'Islam shikka', 'ehsan', 'Yes'),
(4, 'addfsfs', 'sdfsdfsdf', 'No'),
(5, 'sdfsdfsdf', 'sfsdfsdfsd', 'Yes'),
(6, 'fsdfsdfsdf', 'dfsdfsdf', 'No'),
(7, 'sdfsdfsdf', 'sdfsfsd', 'Yes'),
(8, 'ssdfdsfdsf', 'sdfsdf', 'Yes'),
(9, 'sdfdsfdsf', 'sfsdfsf', 'No'),
(10, 'dfdsfdsf', 'dfdsfdsf', 'Yes'),
(11, 'sdfdsfdsf', 'dfsdfsdfsfd', 'Yes'),
(12, 'srerewerwer', 'werewrwe', 'No'),
(13, 'sdfdsfdsfds', 'sdfdsfdsf', 'No'),
(14, 'sdfdsfdsf', 'ssdfdsfd', 'No'),
(16, 'computer', 'fairooz', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `brith_day`
--

CREATE TABLE `brith_day` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `date_of_brith` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brith_day`
--

INSERT INTO `brith_day` (`id`, `name`, `date_of_brith`, `soft_deleted`) VALUES
(1, 'didare', '2017-02-15', 'No'),
(2, 'arif', '2016-09-13', 'Yes'),
(3, 'belal', '2017-02-15', 'No'),
(4, 'ehsan', '2017-02-09', 'No'),
(5, 'kaiom', '2017-02-15', 'No'),
(6, 'didare', '2017-02-20', 'No'),
(7, 'sdfdsfdsfds', '2017-02-07', 'No'),
(8, 'dsfdsfsf', '2017-02-15', 'Yes'),
(9, 'sdfsfs', '2017-02-16', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city_loaction`
--

CREATE TABLE `city_loaction` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `city` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city_loaction`
--

INSERT INTO `city_loaction` (`id`, `name`, `city`, `soft_deleted`) VALUES
(1, 'ehsan', 'Dhaka', 'No'),
(2, 'didar', 'Dhaka', 'No'),
(3, 'belal', 'Borisal', 'No'),
(4, 'arif', 'Comilla', 'Yes'),
(5, 'rony', 'Chittagong', 'No'),
(7, 'sdfdsfsf', 'Rajsahi', 'Yes'),
(8, 'sdfsdfdsf', 'Khulna', 'No'),
(9, 'sdfdsfdsfds', 'Dhaka', 'Yes'),
(10, 'sdfdsfsdf', 'Borisal', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email_address`
--

CREATE TABLE `email_address` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_address`
--

INSERT INTO `email_address` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'didar', 'didar@gmail.com', 'No'),
(3, 'belal', 'belal@gmail.com', 'No'),
(4, 'arif1', 'arif@gmail.com', 'No'),
(5, 'zakir', 'zakir@gmail.com', 'No'),
(6, 'dfsdfds', 'sdfdsfsdf', 'No'),
(7, 'sdfdsfdsf', 'sdfdsfsdf', 'No'),
(8, 'sdfdsfdsf', 'sdfdsfdsf', 'Yes'),
(9, 'sdfdsfdsf', 'sdfdsf', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender_info`
--

CREATE TABLE `gender_info` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `gender` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gender_info`
--

INSERT INTO `gender_info` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(1, 'didar', 'Male', 'No'),
(2, 'fsdfdsf', 'Male', 'No'),
(3, 'sdfdsf', 'Female', 'No'),
(4, 'dfsfdsfdsf', 'Female', 'No'),
(5, 'sdfdsfdsf', 'Female', 'No'),
(6, 'sfsdfsd', 'Femail', 'Yes'),
(7, 'sdfsdfsd', 'Male', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies_info`
--

CREATE TABLE `hobbies_info` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `hobbies` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `picture` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `picture`, `soft_deleted`) VALUES
(1, 'didar', '1486395569bgimage.jpg', 'No'),
(2, 'fsdfdsf', '1486395615bgimage.jpg', 'No'),
(3, 'dsfdsf', '1486395576bgimage.jpg', 'Yes'),
(4, 'fdsfsf', '1486395586bgimage.jpg', 'No'),
(5, 'ddsfsdf', '1486395540bgimage.jpg', 'No'),
(6, 'efsdf', '1486395550bgimage.jpg', 'No'),
(7, 'xfddsfds', '1486395558bgimage.jpg', 'No'),
(8, 'didar', '1486486232atomicbody.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_org`
--

CREATE TABLE `summary_of_org` (
  `id` int(11) NOT NULL,
  `org_name` varchar(111) NOT NULL,
  `summary` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `summary_of_org`
--

INSERT INTO `summary_of_org` (`id`, `org_name`, `summary`, `soft_deleted`) VALUES
(1, 'helth care', 'lsdfsldjfsdf;dskf;ksdf  sdkf;dsjfsdfjsdf sf sefnsdjfjejsf ', 'No'),
(2, 'dsfsdfsdf', 'sdfsdfsdfsdfsdfsdfsdfdsfsdfsdfsdf', 'Yes'),
(3, 'sdfdsfsdfsdf', 'fsdfsdfdsfdsf', 'No'),
(4, 'cricket', 'the cricket of bangladesh', 'No'),
(5, 'sdfdsfsd', 'fsdfdsf', 'No'),
(6, 'sdfdsfdsf', '  sdfdsfdsf  dfdfd   ', 'No'),
(7, 'sdfdsfdsf', 'sdfsdfdsf', 'Yes'),
(8, 'dfdsfdsf', '  werwerewerewe  ', 'No'),
(9, 'ewerwrwer', 'werewrweer', 'No'),
(10, 'weerewrwer', 'weerewerwe', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brith_day`
--
ALTER TABLE `brith_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_loaction`
--
ALTER TABLE `city_loaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_address`
--
ALTER TABLE `email_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender_info`
--
ALTER TABLE `gender_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies_info`
--
ALTER TABLE `hobbies_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_org`
--
ALTER TABLE `summary_of_org`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `brith_day`
--
ALTER TABLE `brith_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `city_loaction`
--
ALTER TABLE `city_loaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `email_address`
--
ALTER TABLE `email_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gender_info`
--
ALTER TABLE `gender_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hobbies_info`
--
ALTER TABLE `hobbies_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `summary_of_org`
--
ALTER TABLE `summary_of_org`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

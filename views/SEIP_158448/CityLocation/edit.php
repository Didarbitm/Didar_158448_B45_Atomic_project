

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";


$objCityLocation = new \App\CityLocation\CityLocation();
$objCityLocation->setData($_GET);
$oneData = $objCityLocation->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Location Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

       <h2> Edit City Location:</h2>
    <form class="form-group" action="update.php" method="post">
        <label>Name:</label>
        <input class="form-control" type="text" name="Name" value="<?php echo $oneData->name?>">
    <br>

        <select name="City"  >
            <option><?php echo $oneData->city?></option>
            <option value="Dhaka">Dhaka</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Khulna">Khulna</option>
            <option value="Rajsahi">Rajsahi</option>
            <option value="Rajsahi">Comilla</option>
            <option value="Borisal">Borisal</option>
            <option value="Rangpur">Rangpur</option>

        </select>
     <br><br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input class="form-control btn btn-primary" type="submit" value="Update">

</form>
   </div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>



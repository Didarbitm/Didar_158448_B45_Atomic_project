

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Location - Single List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>

<?php
require_once("../../../vendor/autoload.php");

$objCityLocation = new \App\CityLocation\CityLocation();
$objCityLocation->setData($_GET);
$oneData = $objCityLocation->view();


echo "
  <div class='container'>
    <h2> Single City Information  </h2>
    <table class='table table-striped table-bordered' cellspacing='0px'>

       <tr>
           <td>ID: </td>
           <td> $oneData->id </td>
       </tr>


       <tr>
           <td>Name: </td>
           <td> $oneData->name </td>
       </tr>

          <tr>
           <td>Email: </td>
           <td> $oneData->city </td>
       </tr>
    </table>

  </div>

";

?>

</body>
</html>

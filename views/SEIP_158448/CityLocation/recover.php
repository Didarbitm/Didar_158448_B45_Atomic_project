<?php
require_once("../../../vendor/autoload.php");


use \App\CityLocation\CityLocation;


$objCityLocation = new CityLocation();

$objCityLocation->setData($_GET);
$objCityLocation->recover();

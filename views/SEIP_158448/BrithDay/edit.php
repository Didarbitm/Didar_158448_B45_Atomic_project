

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";


$objBrithDay = new \App\BrithDay\BrithDay();
$objBrithDay->setData($_GET);
$oneData = $objBrithDay->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Brith Date Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

       <h2> Edit Brith Date:</h2>
    <form class="form-group" action="update.php" method="post">
        <label>Name:</label>
        <input class="form-control" type="text" name="Name" value="<?php echo $oneData->name?>">
    <br>
        <label>Date Of Brith:</label>
        <input class="form-control" type="date" name="dateOfBrith" value="<?php echo $oneData->email?>">
     <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input class="form-control btn btn-primary" type="submit" value="Update">

</form>
   </div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>



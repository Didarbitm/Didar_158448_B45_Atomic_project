

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email Address - Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>

<?php
require_once("../../../vendor/autoload.php");

$objBrithDay = new \App\BrithDay\BrithDay();
$objBrithDay->setData($_GET);
$oneData = $objBrithDay->view();


echo "
  <div class='container'>
    <h2> Single Birday Information  </h2>
    <table class='table table-striped table-bordered' cellspacing='0px'>

       <tr>
           <td>ID: </td>
           <td> $oneData->id </td>
       </tr>


       <tr>
           <td>Name: </td>
           <td> $oneData->name </td>
       </tr>

          <tr>
           <td>Date of Brith: </td>
           <td> $oneData->date_of_brith </td>
       </tr>
    </table>

  </div>

";

?>

</body>
</html>

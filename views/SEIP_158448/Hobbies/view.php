

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email Address - Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <style>

        .viewdiv{

            max-width: 700px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 25px;
            border: solid white 2px;
            border-radius: 5px;
            background-color: #85B5CB;
        }
        .viewdiv td{
            font-weight: bold;
        }
        .viewdiv h2{

            background-color:#85B5CB;
            padding-top: 15px;
            padding-bottom: 15px;
            margin:0;
            color: #C0E2F5;
        }
    </style>

</head>
<body>

<?php
require_once("../../../vendor/autoload.php");

$objHobbies = new \App\Hobbies\Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();


echo "
  <div class='container'>
   <div class='viewdiv'>
    <h2> Single Email Information  </h2>
    <table class='table table-striped table-bordered' cellspacing='0px'>

       <tr>
           <td>ID: </td>
           <td> $oneData->id </td>
       </tr>


       <tr>
           <td>Name: </td>
           <td> $oneData->name </td>
       </tr>

          <tr>
           <td>Hobbies: </td>
           <td> $oneData->hobbies </td>
       </tr>
    </table>
    </div>

  </div>

";

?>

</body>
</html>



<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";


$objHobbies = new \App\Hobbies\Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();
$hobbiesAry= explode(",",$oneData->hobbies);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

       <h2> Hobbies list:</h2>
    <form class="form-group" action="update.php" method="post">
        <label>Name:</label>
        <input class="form-control" type="text" name="Name" value="<?php echo $oneData->name?>">
    <br>
        <label>Hobbies:</label>
        <input  type="checkbox" name="Hobbies[0]" value="Gardening" <?php echo (in_array('Gardening',$hobbiesAry)=='Gardening')?'checked':'' ?> > Gardening
        <input  type="checkbox" name="Hobbies[1]" Value="Collecting Books" <?php echo (in_array('Collecting Books',$hobbiesAry)=='Collecting Books')?'checked':'' ?>> Collecting Books
        <input  type="checkbox" name="Hobbies[2]" Value="Bike Riding" <?php echo (in_array('Bike Riding',$hobbiesAry)=='Bike Riding')?'checked':'' ?>> Bike Riding
        <input  type="checkbox" name="Hobbies[3]" Value="Gaming"<?php echo (in_array('Gaming',$hobbiesAry)=='Gaming')?'checked':'' ?>> Gaming
        <input  type="checkbox" name="Hobbies[4]" Value="Travaling"<?php echo (in_array('Travaling',$hobbiesAry)=='Travaling')?'checked':'' ?>> Travaling
     <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input class="form-control btn btn-primary" type="submit" value="Update">

</form>
   </div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>



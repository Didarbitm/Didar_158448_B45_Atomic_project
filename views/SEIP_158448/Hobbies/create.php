

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Create Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">
    <h2>Input Name and Hobbies:</h2>

<form class="form-group" action="store.php" method="post">

    <label>Name:</label>
    <input class="form-control col-sm-10" type="text" name="Name" placeholder="Enter name">
    <br>
    <label>Hobbies:</label>
    <input  type="checkbox" name="Hobbies[0]" value="Gardening" > Gardening
    <input  type="checkbox" name="Hobbies[1]" Value="Collecting Books"> Collecting Books
    <input  type="checkbox" name="Hobbies[2]" Value="Bike Riding"> Bike Riding
    <input  type="checkbox" name="Hobbies[3]" Value="Gaming"> Gaming
    <input  type="checkbox" name="Hobbies[4]" Value="Travaling"> Travaling
    <br>
    <input type="submit" class="btn btn-primary">

</form>
</div>



<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>



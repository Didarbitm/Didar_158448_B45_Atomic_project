

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Summary of ORG - Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>

<?php
require_once("../../../vendor/autoload.php");

$objBrithDay = new \App\SummaryOfOrganization\SummaryOfOrganization();
$objBrithDay->setData($_GET);
$oneData = $objBrithDay->view();


echo "
  <div class='container'>
    <h2> Single Summary Information  </h2>
    <table class='table table-striped table-bordered' cellspacing='0px'>

       <tr>
           <td>ID: </td>
           <td> $oneData->id </td>
       </tr>


       <tr>
           <td>ORG Name: </td>
           <td> $oneData->org_name </td>
       </tr>

          <tr>
           <td>Summary of ORG: </td>
           <td> $oneData->summary </td>
       </tr>
    </table>

  </div>

";

?>

</body>
</html>

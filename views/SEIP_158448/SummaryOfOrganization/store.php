<?php
require_once("../../../vendor/autoload.php");


use \App\SummaryOfOrganization\SummaryOfOrganization;

$objSummaryOfOrg = new SummaryOfOrganization();

$objSummaryOfOrg->setData($_POST);

$objSummaryOfOrg->store();

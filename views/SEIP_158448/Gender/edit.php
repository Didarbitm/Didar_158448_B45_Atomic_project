

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";


$objGender = new \App\Gender\Gender();
$objGender->setData($_GET);
$oneData = $objGender->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

       <h2> Gender list:</h2>
    <form class="form-group" action="update.php" method="post">
        <label>Name:</label>
        <input class="form-control" type="text" name="Name" value="<?php echo $oneData->name?>">
    <br>
        <label>Gender:</label>
        <input type="radio" name="Gender" value="Male" <?php echo ($oneData->gender=='Male')?'checked':'' ?>> Male
        <input type="radio" name="Gender" value="Female" <?php echo ($oneData->gender=='Female')?'checked':'' ?>> Female
     <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input class="form-control btn btn-primary" type="submit" value="Update">

</form>
   </div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>



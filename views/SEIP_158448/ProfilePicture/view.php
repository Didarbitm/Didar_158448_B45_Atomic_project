

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email Address - Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css" type="text/css">
    <link rel="stylesheet" href="../../../resource/custom_css/style.css">

    <style>
        .viewdiv{

            max-width: 700px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 25px;
            border: solid white 2px;
            border-radius: 5px;
            background-color: #85B5CB;
        }
        .viewdiv td{
            font-weight: bold;
        }
        .viewdiv h2{

            background-color:#85B5CB;
            padding-top: 15px;
            padding-bottom: 15px;
            margin:0;
            color: #C0E2F5;
        }


    </style>


</head>
<body>

<?php
require_once("../../../vendor/autoload.php");

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();
$objProfilePicture->setData($_GET);
$oneData = $objProfilePicture->view();


echo "
  <div class='container'>

    <div class='viewdiv'>
    <h2> Single Profile Picture Information  </h2>
    <table class='table table-striped table-bordered' cellspacing='0px'>

       <tr>
           <td>ID: </td>
           <td> $oneData->id </td>
       </tr>


       <tr>
           <td>Name: </td>
           <td> $oneData->name </td>
       </tr>

          <tr>
           <td>Profile Picture: </td>
           <td><img src='image/$oneData->picture' class='img-thumbnail' alt='image' height='200px' width='250px'> </td>
       </tr>
    </table>
    </div>

  </div>

";

?>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
